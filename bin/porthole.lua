
--[[
display server: (porthole)
	handles drawing pixels to screen(s)
	merges the resolution of all screens into one big frame buffer
	handles screen viewports into the frame buffer
	converts screen events into coordinates relative to the frame buffer
	binds a GPU to a screen
	stores a static framebuffer
	
	events:
		PORTHOLE_FRAMEBUFFER_RESOLUTION_CHANGE
		PORTHOLE_TOO_MANY_SCREENS
		PORTHOLE_TOUCH
		PORTHOLE_DRAG
		PORTHOLE_DROP
		PORTHOLE_SCROLL
		PORTHOLE_WALK
	
	TEMPORARY: only supports one screen	
	
]]

local fs = require("filesystem")

-- make sure screen events are locked to root only
event.lock("touch")
event.lock("drag")
event.lock("drop")
event.lock("scroll")
event.lock("walk")
event.lock("PORTHOLE_FRAMEBUFFER_RESOLUTION_CHANGE")
event.lock("PORTHOLE_TOO_MANY_SCREENS")
event.lock("PORTHOLE_SCREEN_BOUND")
event.lock("PORTHOLE_REDRAW") -- discard current framebuffer and re-draw entire screen contents
event.lock("PORTHOLE_TOUCH")
event.lock("PORTHOLE_DRAG")
event.lock("PORTHOLE_DROP")
event.lock("PORTHOLE_SCROLL")
event.lock("PORTHOLE_WALK")

-- TEMPORARY
local gpu = component.proxy(component.list("gpu")())
gpu.setResolution(gpu.maxResolution())

local framebufferWidth, framebufferHeight = gpu.getResolution()

local function setFramebuffer(x, y, char, foreground, background)
	
	if not foreground then foreground = 0xFFFFFF end
	if not background then background = 0x000000 end
	
	gpu.setForeground(foreground)
	gpu.setBackground(background)
	gpu.set(x, y, char)
end

local gpus = {} -- gpu address, screen address
local screens = {} -- screen address, gpu address

fs.makeDirectory("/bin/porthole")

-- forward touch inputs to the correct application
event.listen("touch", function(screenAddress, x, y, button, playerName)
	event.call("PORTHOLE_TOUCH", x, y, button, playerName)
end)

event.listen("drag", function(screenAddress, x, y, button, playerName)
	event.call("PORTHOLE_DRAG", x, y, button, playerName)
end)

event.listen("drop", function(screenAddress, x, y, button, playerName)
	event.call("PORTHOLE_DROP", x, y, button, playerName)
end)

event.listen("scroll", function(screenAddress, x, y, direction, playerName)
	event.call("PORTHOLE_SCROLL", x, y, direction, playerName)
end)

event.listen("walk", function(screenAddress, x, y, playerName)
	event.call("PORTHOLE_WALK", x, y, playerName)
end)

event.listen("component_added", function(address, componentType)
	if componentType == "gpu" then gpuAdded(address)
	elseif componentType == "screen" then screenAdded(address) end
end)

event.listen("component_removed", function()
	if componentType == "gpu" then gpuRemoved(address)
	elseif componentType == "screen" then screenRemoved(address) end
end)

local porthole = {}
function porthole.getFramebufferSize() return framebufferWidth, framebufferHeight end
function porthole.getScreenViewport(screenAddress)
	-- returns x, y, width, height
end
function porthole.set(x, y, text, foreground, background)
	--gpu.setForeground(foreground)
	--gpu.setBackground(background)
	--gpu.set(x, y, text)
	for tx = 1, #text do
		setFramebuffer(x + tx - 1, y, text:sub(tx,tx), foreground, background)
	end
end
function porthole.fill(x, y, width, height, char, foreground, background)
	--gpu.setForeground(foreground)
	--gpu.setBackground(background)
	--gpu.fill(x, y, width, height, char)
	for tx = x, width + x - 1 do
		for ty = y, height + y - 1 do
			setFramebuffer(tx, ty, char, foreground, background)
		end
	end
end
function porthole.copy(x, y, width, height, tx, ty) -- TODO: FIX ME

	--gpu.copy(x, y, width, height, tx, ty)
	local copy = {}
	for ix = x, width + x do
		if not copy[ix] then copy[ix] = {} end
		for iy = y, height + y do
			if not copy[ix][iy] then copy[ix][iy] = {} end
			
			copy[ix][iy]["c"] = framebuffer[ix][iy]["c"]
			copy[ix][iy]["f"] = framebuffer[ix][iy]["f"]
			copy[ix][iy]["b"] = framebuffer[ix][iy]["b"]
		end
	end
	
	for ix, t1 in ipairs(copy) do
		for iy, t2 in ipairs(t) do
			setFramebuffer(ix, iy, t2["c"], t2["f"], t2["b"])
		end
	end
	
end
function porthole.get(x, y)
	-- returns character, foreground, background or nil for all if they don't exist
	if not framebuffer[x] then return nil, nil, nil end
	if not framebuffer[x][y] then return nil, nil, nil end
	return framebuffer[x][y]
end


message.listen("porthole", function(pid, func, ...)
	return porthole[func](...)
end)

