
--[[ NOTES

EVENTS
	events can be locked to root through the event.lock() function (only usable by root-user programs), and unlocked through event.unlock()
	events that are locked cannot be registered or changed by unprivileged applications

DRIVERS
	because of the new event system, drivers should be run as normal applications but started at boot time and their events locked to root if appropriate
	drivers should register their APIs through message.listen

]]

-- remove access to OpenComputers and Lua functions
local com = {}
com.pullSignal = computer.pullSignal
com.pushSignal = computer.pushSignal
com.shutdown = computer.shutdown
computer.addUser = function() end
computer.pullSignal = function() end
computer.pushSignal = function() end
computer.removeUser = function() end
computer.shutdown = function()
	-- clean shutdown instead of immediate turn off
end

local result,err = pcall(function()

local context = 0 -- this is the currently running program's PID (Process ID)

-- applications
local appsStarted = 0
local apps = {}
app = {}

-- apps[pid] = {}
-- apps[pid]["coroutine"] = coroutine
-- apps[pid]["root"] = boolean, whether the application has root privileges
-- apps[pid]["args"] = arguments passed to application at run time
-- apps[pid]["cpu"] = amount of cpu time in milliseconds that the application used this cycle
-- apps[pid]["events"] = {} events that are registered by this application
-- apps[pid]["ipc"] = name, whether the app has registered with the messaging API

-- components


local compon = {}
compon.invoke = component.invoke
compon.list = component.list
compon.proxy = component.proxy

function component.invoke(...)
	if app.isRoot(app.running()) then return compon.invoke(...) end
	return nil, "Permission denied"
end
function component.list(...)
	if app.isRoot(app.running()) then return compon.list(...) end
	return nil, "Permission denied"
end
function component.proxy(...)
	if app.isRoot(app.running()) then return compon.proxy(...) end
	return nil, "Permission denied"
end

-- events
local events = {}
-- events["event name"] = {}
-- events["event name"]["locked"] = nil
-- events["event name"][id] = {}
-- events["event name"][id]["f"] = function
-- events["event name"][id]["c"] = context

event = {}
function event.listen(e, f)
	
	if not e then return false, "Event must not be nil" end
	if type(e) ~= "string" then return false, "Event must be a string" end
	if not f then return false, "Function must not be nil" end
	if type(f) ~= "function" then return false, "Second argument must be a Function" end
	if events[e] and events[e]["locked"] and not app.isRoot(app.running()) then return false, "Permission denied" end
	
	if not events[e] then events[e] = {} end
	
	table.insert(events[e], {})
	local id = #events[e]
	events[e][id]["f"] = f
	events[e][id]["c"] = context
	
	apps[context]["events"][id] = e
	
	return true, id
end
function event.lock(e)
	
	if not app.isRoot(app.running()) then return false, "Permission denied" end
	if not e then return false, "Event must not be nil" end
	if type(e) ~= "string" then return false, "Event must be a string" end
	
	if not events[e] then events[e] = {} end
	events[e]["locked"] = true
	
	return true
	
end
function event.unlock(e)
	
	if not app.isRoot(app.running()) then return false, "Permission denied" end
	if not e then return false, "Event must not be nil" end
	if type(e) ~= "string" then return false, "Event must be a string" end
	
	if events[e] then events[e]["locked"] = nil end
	--if #events[e] == 0 then events[e] = nil end
	return true
	
end
function event.unlisten(e, id)
	
	if not e then return false, "Event must not be nil" end
	if type(e) ~= "string" then return false, "Event must be a string" end
	if not events[e][context] then return false, "Event not registered" end
	
	if not id then
		events[e][context] = nil
		apps[context]["events"][e] = nil
		return true
	elseif events[e][context][id] then
		events[e][context][id] = nil
		if #events[e][context] == 0 then
			events[e][context] = nil
			apps[context]["events"][e] = nil
		end
		return true
	end
	
	return false, "Event with ID "..tostring(id).." not registered"
end
function event.call(e, ...)
	
	if not e or type(e) ~= "string" then return false, "Event must be a string" end
	if events[e] and events[e]["locked"] and not app.isRoot(app.running()) then return false, "Permission denied" end
	
	-- TODO this needs testing to see whether the application returns to itself properly or whether it goes to another application and never returns
	com.pushSignal(e, ...)
	return true
end
function event.callPid(e, pid, ...)
	
	-- sends an event to a specific application only
	-- useful for sending touch events from window manager to applications
	
	if not e or type(e) ~= "string" then return false, "Event must be a string" end
	if not pid or type(pid) ~= "number" then return false, "PID must be an integer" end
	if events[e] and events[e]["locked"] and not app.isRoot(app.running()) then return false, "Permission denied" end
	
	computer.beep(200)
	com.pushSignal("WINDOMS_CALL_PID", e, pid, ...)
	
	return true
end

-- IPC (Inter-Process Communication)
local ipc = {}
-- ipc[name] = {}
-- ipc[name]["f"] = function
-- ipc[name]["pid"] = pid

message = {}
function message.listen(name, f)
	if not name or type(name) ~= "string" then return false, "Name must be a string." end
	if not f or type (f) ~= "function" then return false, "Callback must be a function." end
	if apps[context]["ipc"] then return false, "App already receiving messages." end
	if ipc[name] then return false, "Name unavailable." end
	
	ipc[name] = {}
	ipc[name]["f"] = f
	ipc[name]["pid"] = context
	
	apps[context]["ipc"] = name

	return true
end
function message.unlisten()
	if not apps[context]["ipc"] then return false, "App is not listening for messages" end
	
	ipc[apps[context]["ipc"]] = nil
	apps[context]["ipc"] = nil
	
	return true
end
function message.send(name, ...)
	
	if not name or type(name) ~= "string" then return false, "Name must be a string" end
	if not ipc[name] then return false, "Name "..name.." not registered" end
	
	-- change context to the application that listens to the message
	local oldcontext = context
	context = ipc[name]["pid"]
	
	local status, response = coroutine.resume(coroutine.create(function(...)
		coroutine.yield(ipc[name]["f"](context, ...)) -- pass the application context through so it can be verified for things like the window manager drawing to apps
	end), ...)
	if not status then response = nil end -- there was an error somewhere
	
	-- return context to the application that sent the message
	context = oldcontext
	return response
end
function message.sendPid(pid, ...) -- exactly the same as message.send but instead it sends to a PID if it has registered for messages
	
	if not pid or type(pid) ~= "number" then return false, "PID must be an integer" end
	if not apps[pid]["ipc"] then return false, "PID "..pid.." not registered" end
	
	return message.send(apps[pid]["ipc"], ...)
	
end

-- sleeping applications
local sleeping = {}
function sleep(seconds)
	if not seconds or type(seconds) ~= "number" then seconds = 0 end

	table.insert(sleeping, {})
	local size = #sleeping
	sleeping[size]["pid"] = app.running()
	sleeping[size]["finish"] = computer.uptime() + seconds
	sleeping[size]["coroutine"] = coroutine.running()

	apps[app.running()]["sleeping"] = apps[app.running()]["sleeping"] + 1

	--[[
	local id = 1
	for k,v in pairs(sleeping[context]) do
		if k >= id then id = k + 1 end
	end
	
	-- coroutine is known unlike schedule, don't store it
	sleeping[context][id] = computer.uptime() + seconds
	apps[context]["sleeping"][id] = coroutine.running()
	]]
	
	coroutine.yield()
end

-- private application functions
local function cleanup(pid)
	
	-- call app close event before values are wiped
	event.call("app_close", pid)
	
	-- remove event listeners
	local eventsCopy = {}
	for id,e in pairs(apps[pid]["events"]) do
		eventsCopy[e] = id
	end
	for e,id in pairs(eventsCopy) do
		event.unlisten(e, id)
	end
	
	-- remove message listeners
	if apps[pid]["ipc"] then
		ipc[apps[pid]["ipc"]] = nil
	end
	
	-- remove sleep waits
	
	
	-- clear application
	apps[pid] = nil
	
end
local function errorHandler(err)
	event.call("app_error", context, err)
	cleanup()
end

-- public applciation functions
function app.running()
	return context
end
function app.isRoot(pid)
	-- TODO validate pid
	
	return true
	
	--if apps[pid]["root"] then return true end
	--return false
	
end
function app.getArgs() end
function app.getLastCpuTime() end
function app.run(uid, code, args)
	
	-- check current application has the authority to launch the application as this user
	if not uid then return nil, "UID must be an integer" end
	
	-- get new PID
	appsStarted = appsStarted + 1
	local pid = appsStarted
	
	if not code then return nil, "Code cannot be nil" end
	
	if not args then args = {} end
	if type(uid) ~= "number" then return nil, "UID must be an integer" end
	if type(args) ~= "table" then return nil, "Arguments must be a Table" end
	
	--[[
	if not name then
		name = ""
		local c = ""
		for i = 1, #path do
			c = path:sub(i, i)
			if c == "/" or c == "\\" then
				name = ""
			elseif c == "." then break
			else
				name = name..c
			end
		end
	end
	if name == "" then name = path end
	]]
	
	--table.insert(apps, {})
	apps[pid] = {}
	apps[pid]["ppid"] = nil -- parent process ID
	apps[pid]["uid"] = uid
	apps[pid]["args"] = args
	apps[pid]["cpu"] = 0
	apps[pid]["events"] = {}
	
	-- set context after uid is set
	local oldcontext = context
	context = pid
	
	local name = nil
	local returnvalue = nil
	apps[pid]["sleeping"] = 0
	apps[pid]["coroutine"] = coroutine.create(function()
		
		-- event code and app code are separated because sleep() requires another coroutine
		coroutine.resume(coroutine.create(function()
			local result, err = pcall(function()
				event.call("app_load", pid)
				returnvalue = load(code)()
			end, errorHandler)
			
			if not result then
				errorHandler(err)
			elseif #apps[pid]["events"] == 0 and apps[pid]["sleeping"] == 0 then
				cleanup()
			end
			
		end))
		
	end)
	
	coroutine.resume(apps[pid]["coroutine"])
	context = oldcontext
	
	return pid, returnvalue
end
local closeApps = {}
function app.close(pid)
	
	if not pid then pid = app.running() end
	if type(pid) ~= "number" then return false, "PID must be an integer" end
	if not apps[pid] then return false, "PID "..pid.." not running" end
	if app.isRoot(pid) and not app.isRoot(app.running()) then return false, "Permission denied" end -- unprivileged applications cannot stop more privileged applications
	
	-- don't close app now, wait until the app isn't in control so the kernel can close it without returning control to it after cleaning all its value
	table.insert(closeApps, pid)
	
	return true
end

-- TODO ADD USER VALIDATION
function app.getValue(pid, key)
	
	return apps[pid][key]
	
end
function app.setValue(pid, key, value)
	
	-- only allow apps running as root to change values
	-- apps aren't allowed to change their own values cause they could screw things up
	
	if not app.isRoot(app.running()) then return false, "Permission denied" end
	
	apps[pid][key] = value
	
end
function app.list()
	local pids = {}
	
	for k,v in pairs(apps) do
		table.insert(pids, k)
	end
	
	return pids
end

-- event logger
--local eventlogger = {}

local bserr = nil
function bluescreen(err)
	
	if not app.isRoot(app.running()) then return false, "Permission denied" end
	
	bserr = tostring(err)
	return true
end

-- event handler
local function eventhandler(e, ...)
	
	if not e then return end
	if not events[e] then return end
	
	if e == "WINDOMS_CALL_PID" then

		local function eventhandlerpid(e, pid, ...)
			local eventId = 0

			if not apps[pid]["events"][id] then return end -- app might not be listening for this event
			for id,ev in pairs(apps[pid]["events"]) do
				if ev == e then
					eventId = id
					break
				end
			end

			context = pid

			coroutine.resume(coroutine.create(function(...)
				return events[e][eventId]["f"](...)
			end), ...)

			context = 0
		end

		eventhandlerpid(...)
		return
	end

	-- copy events table because event.unlisten could be called while looping the table
	local ev = {}
	for id,t in ipairs(events[e]) do
		
		ev[id] = {}
		
		for k,v in pairs(t) do
			
			ev[id][k] = v
			
		end
		
	end
	
	-- execute events
	for id,t in pairs(ev) do
		
		-- check if the event hasn't been unregistered in the time this loop has been iterating
		if events[e] and events[e][id] then
			
			if apps[t["c"]] then
				context = t["c"]
				
				local cancel = coroutine.resume(coroutine.create(function(...)
					return ev[id]["f"](...)
				end), ...)
				
				--local cancel = coroutine.resume(apps[t["c"]]["coroutine"], "event", ev[id]["f"], ...)
				
				-- allow apps running as root to cancel events
				if cancel and app.isRoot(app.running()) then
					context = 0
					return
				end
			
				context = 0
			end
			
		end
		
	end
	
end

-- run startup app
function readfile(address, file)
	if not app.isRoot(app.running()) then return nil end
	
	local text = ""
	local handle = component.invoke(address, "open", file)
	local data = nil
	repeat
		data = component.invoke(address, "read", handle, math.huge)
		text = text..(data or "")
	until not data
	component.invoke(address, "close", handle)
	return text
end

-- re-implement the require library
local REQUIRE_PATH = "/windoms/libraries/"
function require(library)
	local path = REQUIRE_PATH..library..".lua"
	if not component.invoke(computer.getBootAddress(), "exists", path) then return nil end
	return load(readfile(computer.getBootAddress(), path))()
end

-- run first application
app.run(0, readfile(computer.getBootAddress(), "bin/startup.lua")) -- UID 0 means root

while true do
	
	if bserr then
		error(bserr)
	end
	
	eventhandler(com.pullSignal(0))
	
	if #sleeping > 0 then
		
		--[[
		local s = {}
		for index in ipairs(sleeping) do
			s[index] = {}
			s[index]["pid"] = sleeping[index]["pid"]
			s[index]["finish"] = sleeping[index]["finish"]
			s[index]["coroutine"] = sleeping[index]["coroutine"]

			local gpu = component.proxy(component.list("gpu")())
			gpu.setForeground(0xFFFFFF)
			gpu.setBackground(0x000000)
			gpu.set(1,index,"index: "..tostring(index)..", pid: "..tostring(s[index]["pid"])..", finish: "..tostring(s[index]["finish"])..", coroutine: "..tostring(s[index]["coroutine"]).."             ")

		end
		]]

		local sleepSize = #sleeping
		local removedSleeps = {}
		for i=1,sleepSize do

			if apps[sleeping[1]["pid"]] then

				if computer.uptime() >= sleeping[1]["finish"] then

					context = sleeping[1]["pid"]
					apps[context]["sleeping"] = apps[context]["sleeping"] - 1

					coroutine.resume(sleeping[1]["coroutine"])
					context = 0
	
				else
					table.insert(removedSleeps, sleeping[1])
				end
			end

			table.remove(sleeping, 1)
		end
		for _,s in ipairs(removedSleeps) do table.insert(sleeping, s) end

		--[[
		for index in ipairs(s) do

			if sleeping[index] then -- TODO potentially buggy because of table.remove indexes

				if computer.uptime() >= s[index]["finish"] then
					local pid = s[index]["pid"]
					local coroutine = s[index]["coroutine"]
					
					table.remove(sleeping, index)

					context = pid
					coroutine.resume(coroutine)
					context = 0
				end

			end

		end
		]]

		--[[
		for pid,t in pairs(sleeping) do
			s[pid] = {}
			for id,seconds in pairs(t) do
				s[pid][id] = seconds
			end
		end
		
		for pid,t in pairs(s) do
			
			for id,seconds in pairs(t) do
				if sleeping[pid] and sleeping[pid][id] then
					
					if computer.uptime() >= seconds then
						
						local co = apps[pid]["sleeping"][id]
						sleeping[pid][id] = nil
						apps[pid]["sleeping"][id] = nil
						
						context = pid
						coroutine.resume(co)
						context = 0
						
					end
					
				end
				
			end
			
		end
		]]
		
	end
	
	if #closeApps > 0 then
		
		for _,pid in pairs(closeApps) do
			cleanup(pid)
		end
		
		closeApps = {}
	end
	
end
end)

if not result then

	-- show bluescreen on all available gpus and screens
	for address in component.list("gpu") do
		
		local gpu = component.proxy(address)
		w,h = gpu.maxResolution()
		gpu.setResolution(w,h)
		gpu.setBackground(0x2067B2)
		gpu.setForeground(0xFFFFFF)
		gpu.fill(1,1,w,h," ")
		
		-- old style windows XP bluescreen
		--[[
		gpu.set(1,1,"A problem has been detected and Windoms has been")
		gpu.set(1,2,"shut down to prevent damage to your computer.")
		
		gpu.set(1,4,err)
		
		gpu.set(1,6,"Check to make sure any new hardware or software is")
		gpu.set(1,7,"properly installed. If this is a new installation,")
		gpu.set(1,8,"ask your hardware or software manufacturer for any")
		gpu.set(1,9,"Windoms updates you might need.")
		
		gpu.set(1,11,"If problems continue, disable or remove any newly")
		gpu.set(1,12,"installed hardware or software.")
		]]
		
		-- windows 10 style bluescreen
		gpu.set(5,12,"Your PC ran into a problem and needs to restart. We're just")
		gpu.set(5,13,"collecting some error info, and then we'll restart for you.")
		
		gpu.set(5,17,err)
		
		gpu.setBackground(0xFFFFFF)
		-- eyes
		gpu.fill(5,4,2,1," ")
		gpu.fill(5,7,2,1," ")
		-- mouth
		gpu.fill(9,5,2,2," ")
		gpu.fill(11,4,2,1," ")
		gpu.fill(13,3,2,1," ")
		gpu.fill(11,7,2,1," ")
		gpu.fill(13,8,2,1," ")
		
		gpu.setBackground(0x2067B2)
		
		local start = computer.uptime()
		while true do
			com.pullSignal(0)
			gpu.set(5,15,"("..math.ceil((100 / 10) * (computer.uptime() - start)).."% complete)")
			if computer.uptime() >= start + 10 then break end
		end
		
	end
else com.shutdown()
end
com.shutdown(true) -- restart
