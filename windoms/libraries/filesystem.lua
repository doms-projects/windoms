local MSG_ID = "fs"

local fs = {}

function fs.exists(path) return message.send(MSG_ID, "exists", path) end
function fs.isDirectory(path) return message.send(MSG_ID, "isDirectory", path) end
function fs.read(path) return message.send(MSG_ID, "read", path) end
function fs.write(path, data) return message.send(MSG_ID, "write", path, data) end
function fs.rename(path, newName) return message.send(MSG_ID, "rename", path, newName) end
function fs.makeDirectory(path) return message.send(MSG_ID, "makeDirectory", path) end
function fs.touch(path) return fs.write(path, "") end

return fs
