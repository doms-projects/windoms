local MSG_ID = "shingle"

local shingle = {}

function shingle.create(x, y, width, height) return message.send(MSG_ID, "create", app.running(), x, y, width, height) end
function shingle.getX() return message.send(MSG_ID, "getX", app.running()) end
function shingle.getY() return message.send(MSG_ID, "getY", app.running()) end
function shingle.setPosition(x, y) return message.send(MSG_ID, "setPosition", app.running(), x, y) end
function shingle.getWidth() return message.send(MSG_ID, "getWidth", app.running()) end
function shingle.getHeight() return message.send(MSG_ID, "getHeight", app.running()) end
function shingle.setSize(width, height) return message.send(MSG_ID, "setSize", app.running(), width, height) end
function shingle.isVisible() return message.send(MSG_ID, "isVisible", app.running()) end
function shingle.setVisible(visible) return message.send(MSG_ID, "setVisible", app.running(), visible) end
function shingle.getFrameBuffer() return message.send(MSG_ID, "getFrameBuffer", app.running()) end
function shingle.get(x, y) return message.send(MSG_ID, "get", app.running(), x, y) end
function shingle.set(x, y, text, foreground, background) return message.send(MSG_ID, "set", app.running(), x, y, tostring(text), foreground, background) end
function shingle.fill(x, y, width, height, char, foreground, background) return message.send(MSG_ID, "fill", app.running(), x, y, width, height, char, foreground, background) end
function shingle.getAppVisibleAtScreenPixel(x, y) return message.send(MSG_ID, "getAppVisibleAtScreenPixel", app.running(), x, y) end

return shingle
