--[[
    ABOUT

    API for getting user information
        currently logged in user
        username
        password checker true/false (not for retrieving password)
        other people allowed to interact with the computer on their account
        home directory
        whether the user is allowed to log in (if login is disallowed, applications can still be run as that user)
    
    keeps track of logged in users
]]

local fs = require("filesystem")

local USERS_DIR = "/bin/users"
local USERS_FILE = "/bin/users/users"

fs.makeDirectory(USERS_DIR)

local loadedUsers = {}
-- loadedUsers[uid]["username"]
-- loadedUsers[uid]["password"]
-- loadedUsers[uid]["interact"]
-- loadedUsers[uid]["home"]
-- loadedUsers[uid]["loginallowed"]

--[[
    values are comma separated, and users are semicolon separated (new lines were unnecessarily complicated)
    boolean values are represented as 0 for false, 1 for true

    1: uid
    2: username
    3: password hash (if applicable)
    4: interactions allowed
    5: home directory (if applicable)
    6: login allowed

]]

local function loadUsersFile(file)

    -- create users file if it doesn't exist
    if not fs.exists(file) then fs.touch(file) end



end
local function saveUsersFile(file)

end

loadUsersFile(USERS_FILE)

local users = {}
function users.loggedInUsers()
end
function users.setLoggedIn(uid, loggedIn)

end
function users.getUsername(uid)
end
function users.setUsername(uid)
    saveUsersFile(USERS_FILE)
end
function users.checkPassword(uid, password)
end
function users.setPassword(uid, password)
    -- if password is nil then remove password
    saveUsersFile(USERS_FILE)
end
function users.removePassword(uid) return users.setPassword(uid, nil) end
function users.canInteract(uid)
end
function users.setInteract(uid, interact)
    saveUsersFile(USERS_FILE)
end
function users.isLoginAllowed(uid)
end
function users.setLoginAllowed(uid, allowed)
    saveUsersFile(USERS_FILE)
end
function users.getHomeDirectory(uid)
end
function users.setHomeDirectory(uid, dir)
    saveUsersFile(USERS_FILE)
end

message.listen("users", function(pid, func, ...)
    return users[func](...)
end)