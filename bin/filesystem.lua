
-- filesystems are stored here because component is unavailable when called from an application
local filesystems = {}
-- filesystems[component id] = proxy

local mountpoints = {}
-- mountpoints[directory] = address

local function mountFilesystem(address, path)
	if component.invoke(address, "getLabel") == "tmpfs" then return false end -- TODO tmpfs doesn't like to mount for some reason
	filesystems[address] = component.proxy(address)
	mountpoints[path] = address
	return true
end

mountFilesystem(computer.getBootAddress(), "/")
for address in component.list("filesystem") do
	if address ~= computer.getBootAddress() then mountFilesystem(address, "/mount/"..address) end
end

-- filesystems are mounted upon connection to the computer
-- returns the address of the path provided, and the root mount point
local function getFilesystem(path)
	-- returns the address and actual path of the file or directory
	
	local address = computer.getBootAddress()
	local test = ""
	local mountpath = ""
	if path:sub(1,1) ~= "/" then path = "/"..path end
	for part in (path.."/"):gmatch("(.-)/") do
		
		-- from left to right, figure out which address is currently being accessed
		if test == "/" then test = "" end
		test = test.."/"..part
		if mountpoints[test] then
			address = mountpoints[test]
			mountpath = path:sub(#test + 1, #path)
		end
		
	end
	
	if #mountpath == 0 or mountpath:sub(1,1) ~= "/" then mountpath = "/"..mountpath end -- always has to have a / at the start of the path
	return address, mountpath
end

-- converts a path to just the portion on the mounted filesystem
-- this is a local function to be able to write, read etc files from different drives but is invisible to the user
local function getFilesystemLocalPath(path)
	
	local address, mountpoint = getFilesystem(path)
	
	-- remove mountpoint from start of path to get local path
	path = path:sub(1,#mountpoint)
	
	return address, path
	
end

local f = {}
function f.exists(path)
	
	local address, localPath = getFilesystemLocalPath(path)
	
	local result, err = pcall(function()
		return filesystems[address].exists(localPath)
	end)
	
	if not result then return nil, err end
	return result
end
function f.isDirectory(path)

	local address, localPath = getFilesystemLocalPath(path)
	local result, err = pcall(function()
		return filesystems[address].isDirectory(localPath)
	end)
	
	if not result then return nil, err end
	return result

end
function f.read(path)
	
	local address, localPath = getFilesystemLocalPath(path)
	local result, err = pcall(function()
		local text = ""
		local handle = filesystems[address].open(localPath, "r")
		local data = nil
		repeat
			data = filesystems[address].read(handle, math.huge)
			text = text..(data or "")
		until not data
		return text
	end)
	
	if not result then return nil, err end
	return result
end
function f.write(path, data)

	local address, localPath = getFilesystemLocalPath(path)
	local result, err = pcall(function()
		local handle = filesystems[address].open(localPath, "w")
		local written = filesystems[address].write(handle, data)
		filesystems[address].close(handle)
		return written
	end)
	
	if not result then return nil, err end
	return result
end
function f.makeDirectory(path)
	
	local address, localPath = getFilesystemLocalPath(path)
	local result, err = pcall(function()
		return filesystems[address].makeDirectory(localPath)
	end)
	
	if not result then return nil, err end
	return result
	
end
function f.rename(path, newName)
	
	local address, localPath = getFilesystemLocalPath(path)
	local result, err = pcall(function()
		local newLocalPath = localPath.sub(#localPath - #newname, #localPath)
		return filesystems[address].rename(localPath, newLocalPath)
	end)
	
	if not result then return nil, err end
	return result
	
end
function f.lastModified(path) end
function f.remove(path) end
function f.size(path) end

function f.isFilesystemWritable(address) return true end
function f.filesystemSpaceTotal(address) end
function f.filesystemSpaceFree(address) end
function f.filesystemSpaceUsed(address) end
function f.getFilesystemLabel(address) end
function f.setFilesystemLabel(address, label) end
function f.mountFilesystem(address, directory) end
function f.unmountFilesystem(address) end
function f.listFilesystems() end

message.listen("fs", function(pid, func, ...)
	
	if not f[func] then return nil end
	
	return f[func](...)
	
end)

