
--[[
	
	ABOUT
	
	locks the component events to root so that normal applications can't take over a component
	
]]

event.lock("component_added")
event.lock("component_removed")