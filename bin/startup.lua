
-- lock startup event to root only
event.lock("startup")

local startupApps = {}
local startupFile = readfile(computer.getBootAddress(), "startup.txt")

local nextApp = ""
for i = 1, #startupFile do
	local c = startupFile:sub(i, i)
	if c == ";" then
		table.insert(startupApps, nextApp)
		nextApp = ""
	else
		nextApp = nextApp..c
	end
end

local function startApp(path)
	
	local pid = app.run(0, readfile(computer.getBootAddress(), path))
	event.call("startup", pid, #startupApps, path) -- pid, total apps, app path
	
end

event.listen("app_error", function(context, err)
	bluescreen(err)
end)

for _,path in pairs(startupApps) do
	startApp(path)
	sleep(0.1)
end

event.call("startup_finished")

app.close(app.running())
