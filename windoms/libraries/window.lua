
--[[
	
	ABOUT
	
	creates the app title bar
	internally moves the app coordinates to below the title bar so it's not overwritten
	can disable title bar if you want (then coordinates return to 1,1 at top left instead of 1,2)
	
]]

local shingle = require("shingle")

local displayTitleBar = true
local yOffset = 1

shingle.create(1, 1, 10, 7)

local window = {}

function window.displayTitleBar(display)
	if not display or type(display) ~= "boolean" then return nil, "Argument must be boolean." end
	local oldValue = displayTitleBar
	displayTitleBar = display
	return oldValue
end
function window.set(x, y, text, foreground, background, alpha) return shingle.set(x, y, text, foreground, background, alpha) end
function window.fill(x, y, width, height, char, foreground, background, alpha) return shingle.fill(x, y, width, height, char, foreground, background, alpha) end
function window.copy() end
function window.get(x, y) return shingle.get(x, y) end
function window.getSize()
	local width = shingle.getWidth()
	local height = shingle.getHeight()
	return width, height
end
function window.setSize(width, height) return shingle.setSize(width, height) end
function window.getPosition()
	local x = shingle.getX()
	local y = shingle.getY()
	return x, y
end
function window.setPosition(x, y) return shingle.setPosition(x, y) end
function window.isVisible() return shingle.isVisible() end
function window.setVisible(visible) return shingle.setVisible(visible) end

return window
