
--[[
window manager: (shingle)
	interacts with display server to draw pixels
	stores frame buffer data for each application (if any)
	forwards screen events to the relevant application
	
	NOTES:
		100% transparency should pass events through to the app behind (useful for taskbar)
		apps can specify if they are always on top or always behind
			always behind is useful for the desktop wallpaper but has icons and right-click options
		needs to stop printing text at the application width
			use drawPixel which automatically converts window to screen pixels and doesn't draw invisible
	
]]

local porthole = require("porthole")

local windows = {} -- list of applications and their details and frame buffer
--windows[pid] = {}
--windows[pid]["pixels"][x][y] = {}
--windows[pid]["pixels"][x][y]["c"] = character
--windows[pid]["pixels"][x][y]["f"] = foreground
--windows[pid]["pixels"][x][y]["b"] = background
--windows[pid]["pixels"][x][y]["a"] = alpha (0 to 1)
--windows[pid]["visible"] = boolean
--windows[pid]["x"] = top left window X position
--windows[pid]["y"] = top left window Y position
--windows[pid]["width"] = window width
--windows[pid]["height"] = window height
local windowOrder = {} -- list of application PIDs and their order in the list; highest id = top window; windowOrder[id] = pid

local function convertWindowToScreenCoordinates(pid, x, y)
	return x + windows[pid]["x"] - 1, y + windows[pid]["y"] - 1
end

-- returns values if the specified window is at the screen coordinates
function convertScreenToWindowCoordinates(pid, screenX, screenY)

	-- check if screen coordinates are within the window bounds
	if screenX >= windows[pid]["x"] and screenX < windows[pid]["x"] + windows[pid]["width"] then
		if screenY >= windows[pid]["y"] and screenY < windows[pid]["y"] + windows[pid]["height"] then

			return screenX - windows[pid]["x"] + 1, screenY - windows[pid]["y"] + 1

		end
	end
end

-- returns: pid, index of window order, X pos inside window, Y pos inside window
function getAppVisibleAtScreenPixel(screenX, screenY)

	for i = #windowOrder, 1, -1 do -- descending order
		local pid = windowOrder[i]

		-- ignore window if it's invisible
		if windows[pid]["visible"] then

			local windowX, windowY = convertScreenToWindowCoordinates(pid, screenX, screenY)
			
			if windowX and windows[pid]["pixels"][windowX][windowY]["a"] > 0 then return pid, i, windowX, windowY end
		end
	end
end

local function drawPixel(pid, windowX, windowY, char, foreground, background, alpha)
	if windowX < 1 then return end
	if windowY < 1 then return end
	if windowX > windows[pid]["width"] then return end
	if windowY > windows[pid]["height"] then return end

	if not windows[pid]["pixels"][windowX] then windows[pid]["pixels"][windowX] = {} end
	if not windows[pid]["pixels"][windowX][windowY] then windows[pid]["pixels"][windowX][windowY] = {} end
	windows[pid]["pixels"][windowX][windowY]["c"] = char
	windows[pid]["pixels"][windowX][windowY]["f"] = foreground
	windows[pid]["pixels"][windowX][windowY]["b"] = background
	windows[pid]["pixels"][windowX][windowY]["a"] = alpha

	local screenX, screenY = convertWindowToScreenCoordinates(pid, windowX, windowY)

	-- don't draw the window if it's invisible
	if not windows[pid]["visible"] then return end

	-- TEMPORARY don't draw this pixel if it's not in front
	local frontPid = getAppVisibleAtScreenPixel(screenX, screenY)
	if not frontPid or frontPid ~= pid then return end

	-- TEMPORARY don't draw this pixel if its alpha is 0
	if alpha <= 0 then return end

	porthole.set(screenX, screenY, tostring(char), foreground, background)
end

local function drawApp(pid)
	if not windows[pid]["visible"] then return end

	for x in ipairs(windows[pid]["pixels"]) do
		for y in ipairs(windows[pid]["pixels"][x]) do
			
			drawPixel(pid, x, y, windows[pid]["pixels"][x][y]["c"], windows[pid]["pixels"][x][y]["f"], windows[pid]["pixels"][x][y]["b"], windows[pid]["pixels"][x][y]["a"])
			
		end
	end
end

local function bringWindowToFront(pid)
	if windowOrder[#windowOrder] == pid then return end -- window is already on top
	if not windows[pid]["focusable"] then return end -- window has denied focus

	local windowOrderCopy = {}
	for index,processId in ipairs(windowOrder) do windowOrderCopy[index] = processId end
	for index,processId in ipairs(windowOrderCopy) do
		if processId == pid then
			table.remove(windowOrder, index)
			table.insert(windowOrder, pid)
			drawApp(pid)
			break
		end
	end

end

local function drawWindowsBelowPid(pid)
	local id = 1
	for windowId, processId in ipairs(windowOrder) do
		if pid == processId then
			id = windowId
			break
		end
	end
	for i = id - 1, 1 do drawApp(windowOrder[i]) end
end

local function removeAppFromWindowOrder(pid)
	local windowId = 1
	local windowOrderCopy = {}
	for index,processId in ipairs(windowOrder) do windowOrderCopy[index] = processId end
	for index,processId in ipairs(windowOrderCopy) do
		if processId == pid then
			windowId = index
			table.remove(windowOrder, pid)
			break
		end
	end
	return windowId
end

local function removeApp(pid)
	if not windows[pid] then return end
	
	windows[pid] = nil
	local windowId = removeAppFromWindowOrder(pid)
	
	-- re-draw applications below this window's ID
	for i = windowId - 1, 1 do drawApp(windowOrder[i]) end
end

local window = {}
function window.create(pid, x, y, width, height)

	if not pid then return nil, "PID must be a number." end
	if not x then x = 1 end
	if not y then y = 1 end
	if not width then width = 15 end
	if not height then height = 10 end
	
	table.insert(windowOrder, pid)

	windows[pid] = {}
	windows[pid]["pixels"] = {}
	windows[pid]["visible"] = true
	windows[pid]["x"] = x
	windows[pid]["y"] = y
	windows[pid]["width"] = width
	windows[pid]["height"] = height
	windows[pid]["focusable"] = true -- when you click on a window, whether or not it should be brought to the front
end
function window.getX(pid)
	if not pid then return nil, "PID must be a number." end
	if not windows[pid] then return nil, "PID does not exist." end
	return windows[pid]["x"]
end
function window.getY(pid)
	if not pid then return nil, "PID must be a number." end
	if not windows[pid] then return nil, "PID does not exist." end
	return windows[pid]["y"]
end
function window.setPosition(pid, x, y)
	if not pid then return nil, "PID must be a number." end
	if not windows[pid] then return nil, "PID does not exist." end
	if not x or type(x) ~= "number" then return nil, "X must be a number." end
	if not y or type(y) ~= "number" then return nil, "Y must be a number." end
	windows[pid]["x"] = x
	windows[pid]["y"] = y
	return true
end
function window.getWidth(pid)
	if not pid then return nil, "PID must be a number." end
	if not windows[pid] then return nil, "PID does not exist." end
	return windows[pid]["width"]
end
function window.getHeight(pid)
	if not pid then return nil, "PID must be a number." end
	if not windows[pid] then return nil, "PID does not exist." end
	return windows[pid]["height"]
end
function window.setSize(pid, width, height)
	if not pid then return nil, "PID must be a number." end
	if not windows[pid] then return nil, "PID does not exist." end
	if not width or type(width) ~= "number" then return nil, "Width must be a number." end
	if not height or type(height) ~= "number" then return nil, "Height must be a number." end
	if width < 1 then return nil, "Width must be above 0." end
	if height < 1 then return nil, "Height must be above 9." end
	windows[pid]["width"] = width
	windows[pid]["height"] = height
	return true
end
function window.isVisible(pid)
	if not pid then return nil, "PID must be a number." end
	if not windows[pid] then return nil, "PID does not exist." end
	return windows[pid]["visible"]
end
function window.setVisible(pid, visible)
	if not pid then return nil, "PID must be a number." end
	if not windows[pid] then return nil, "PID does not exist." end
	if not visible or type(visible) ~= "boolean" then return nil, "Visible must be a boolean." end
	windows[pid]["visible"] = visible
	drawWindowsBelowWindow(pid)
end
function window.getFrameBuffer(pid)
	if not pid then return nil, "PID must be a number." end
	if not windows[pid] then return nil, "PID does not exist." end
	return windows[pid]["pixels"]
end
function window.getPixel(pid, x, y)
	if not pid then return nil, "PID must be a number." end
	if not windows[pid] then return nil, "PID does not exist." end
	if not x or type(x) ~= "number" then return nil, "X must be a number." end
	if not y or type(y) ~= "number" then return nil, "Y must be a number." end
	
	if not windows[pid]["pixels"][x] then return {} end
	if not windows[pid]["pixels"][x][y] then return {} end
	return windows[pid]["pixels"][x][y]
end
function window.set(pid, x, y, text, foreground, background, alpha)
	if not pid then return nil, "PID must be a number." end
	if not windows[pid] then return nil, "PID does not exist." end
	if not x or type(x) ~= "number" then return nil, "X must be a number." end
	if not y or type(y) ~= "number" then return nil, "Y must be a number." end
	if not text or type(text) ~= "string" then return nil, "Text must be a string." end
	
	if not foreground or type(foreground) ~= "number" then foreground = 0xFFFFFF end
	if not background or type(background) ~= "number" then background = 0x000000 end
	if not alpha or type(alpha) ~= "number" then alpha = 1 end
	if alpha < 0 then alpha = 0 end
	
	for i=1,#text do
		drawPixel(pid, x + i - 1, y, text:sub(i,i), foreground, background, alpha)
	end
end
function window.fill(pid, x, y, width, height, char, foreground, background, alpha)
	if not pid then return nil, "PID must be a number." end
	if not windows[pid] then return nil, "PID does not exist." end
	if not x or type(x) ~= "number" then return nil, "X must be a number." end
	if not y or type(y) ~= "number" then return nil, "Y must be a number." end
	if not width or type(width) ~= "number" then return nil, "Width must be a number." end
	if not height or type(height) ~= "number" then return nil, "Height must be a number." end
	
	if not char or type(char) ~= "string" then char = " " end
	if not foreground or type(foreground) ~= "number" then foreground = 0xFFFFFF end
	if not background or type(background) ~= "number" then background = 0x000000 end
	if not alpha or type(alpha) ~= "number" then alpha = 1 end
	if alpha < 0 then alpha = 0 end
	
	for tx=1,width do
		for ty=1,height do
			drawPixel(pid, tx, ty, char, foreground, background, alpha)
		end
	end
end

event.listen("app_close", function(pid)
	-- remove framebuffer if application had a window
	if not windows[pid] then return end
	
	removeApp(pid)
end)

-- if an app crashes, remove it
-- also start the error application and display what went wrong
event.listen("app_error", function(pid, err)
	removeApp(pid)
end)

event.listen("PORTHOLE_TOUCH", function(screenX, screenY, button, playerName)
	-- windowX, windowY, button, playerName, framebufferX, framebufferY

	local pid, _, windowX, windowY = getAppVisibleAtScreenPixel(screenX, screenY)
	if not pid then return end

	bringWindowToFront(pid)
	event.callPid("app_touch", pid, windowX, windowY, button, playerName, screenX, screenY)
end)

event.listen("PORTHOLE_DRAG", function(screenX, screenY, button, playerName)
	
	local pid, _, windowX, windowY = getAppVisibleAtScreenPixel(screenX, screenY)
	if not pid then return end

	bringWindowToFront(pid)
	event.callPid("app_drag", pid, windowX, windowY, button, playerName, screenX, screenY)
end)

event.listen("PORTHOLE_DROP", function(screenX, screenY, button, playerName)

	local pid, _, windowX, windowY = getAppVisibleAtScreenPixel(screenX, screenY)
	if not pid then return end

	bringWindowToFront(pid)
	event.callPid("app_drop", pid, windowX, windowY, button, playerName, screenX, screenY)
end)

event.listen("PORTHOLE_WALK", function(x, y, playerName)
	-- TODO
end)

message.listen("shingle", function(pid, func, ...)
	return window[func](...)
end)

