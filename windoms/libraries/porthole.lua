local MSG_ID = "porthole"

local porthole = {}

function porthole.getFramebufferSize() return message.send(MSG_ID, "getFramebufferSize") end
function porthole.getScreenViewport(screenAddress) return message.send(MSG_ID, "getScreenViewport", screenAddress) end
function porthole.set(x, y, text, foreground, background) return message.send(MSG_ID, "set", x, y, text, foreground, background) end
function porthole.fill(x, y, width, height, char, foreground, background) return message.send(MSG_ID, x, y, width, height, char, foreground, background) end
function porthole.get(x, y) return message.send(MSG_ID, x, y) end

return porthole
