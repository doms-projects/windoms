
local finished = false

--local w,h = 10,7
local g = component.proxy(component.list("gpu")())

local w,h = g.maxResolution()
g.setResolution(w,h)

-- clear screen
g.setBackground(0x000000)
g.fill(1,1,w,h," ")

local y = -2
local sw = 3 -- square width
local sh = 2 -- square height

local function topLeft(colour)
	g.setBackground(colour)
	g.fill(w / 2 - sw, h / 2 - sh + 1 + y, sw, sh, " ")
end
local function topRight(colour)
	g.setBackground(colour)
	g.fill(w / 2 + 2, h / 2 - sh + 1 + y, sw, sh, " ")
end
local function bottomLeft(colour)
	g.setBackground(colour)
	g.fill(w / 2 - sw, h / 2 + 2 + y, sw, sh, " ")
end
local function bottomRight(colour)
	g.setBackground(colour)
	g.fill(w / 2 + 2, h / 2 + 2 + y, sw, sh, " ")
end

--topLeft(0xFFFFFF)
--topRight(0xFFFFFF)
--bottomLeft(0xFFFFFF)
--bottomRight(0xFFFFFF)

event.listen("startup", function(loaded, total, path)
	
	-- loading bar
	g.setBackground(0xFFFFFF)
	g.fill(math.floor(w / 2) - 3, h / 2 + 5 + y, math.floor(8 / total * loaded), 1, " ")
	
	-- display file being loaded
	g.setBackground(0x000000)
	g.setForeground(0xFFFFFF)
	g.fill(1, h / 2 + 7 + y, w, 1, " ")
	g.set(math.floor(w / 2) - (#path / 2) + 2, h / 2 + 7 + y, path)
	
end)

-- close itself when apps are finished starting up
event.listen("startup_finished", function()
	
	finished = true
	
	-- clear screen
	g.setBackground(0x000000)
	g.fill(1,1,w,h," ")
	
	app.close(app.running())
	
end)

local anim = 0
while true do
	if not finished then
		
		if anim == 1 then
			
			topLeft(0xFFFFFF)
			topRight(0x000000)
			
		elseif anim == 2 then
			
			topRight(0xFFFFFF)
			bottomRight(0x000000)
			
		elseif anim == 3 then
			
			bottomRight(0xFFFFFF)
			bottomLeft(0x000000)
			
		elseif anim == 4 then
			
			bottomLeft(0xFFFFFF)
			topLeft(0x000000)
		
		end
		
		if anim >= 4 then anim = 0 end
		
		anim = anim + 1
	end
	sleep(0.35)
end

