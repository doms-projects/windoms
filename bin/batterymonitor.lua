
local BATTERY_EVENT = "battery_level_change"

local batteryLevel = 0

local charging = false

local battery = {}
function battery.remaining() return batteryLevel end
function battery.maximum() return computer.maxEnergy() end
function battery.isCharging() return charging end

message.listen("battery", function(pid, func, ...)
	if not battery[func] then return nil end
	return battery[func](...)
end)

while true do
	
	local newBatteryLevel = computer.energy()
	local oldBatteryLevel = batteryLevel
	batteryLevel = newBatteryLevel
	if newBatteryLevel > oldBatteryLevel then
		charging = true
		message.send(BATTERY_EVENT, batteryLevel, charging)
	elseif newBatteryLevel < batteryLevel then
		charging = false
		message.send(BATTERY_EVENT, batteryLevel, charging)
	end
	sleep(1)
end
